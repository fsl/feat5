#!/bin/sh

#   estnoise - quick and dirty estimate of (temporal) noise in 4D data
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 2005-2007 University of Oxford
#
#   SHCOPYRIGHT

Usage() {
    echo "Usage: estnoise <4d_input_data> [spatial_sigma temp_hp_sigma temp_lp_sigma]"
    exit 1
}

[ "$1" = "" ] && Usage
[ `${FSLDIR}/bin/imtest $1` = 0 ] && Usage

IPOPTS=""
SMOOTHOPTS="-kernel gauss $2"
if [ "$4" != "" ] ; then
    IPOPTS="$SMOOTHOPTS -fmean -bptf $3 $4"
fi

IN=`${FSLDIR}/bin/remove_ext $1`
TMP=`${FSLDIR}/bin/tmpnam /tmp/estnoise`

# create mask for valid voxels above 20% threshold and erode a little to avoid edge voxels
${FSLDIR}/bin/fslmaths $IN -thrp 20 -Tmin -bin -ero -ero ${TMP}_mask -odt char

# create initial temporal mean image
${FSLDIR}/bin/fslmaths $IN -Tmean ${SMOOTHOPTS} ${TMP}_mean

# do basic spatiotemporal filtering ( restoring mean )
${FSLDIR}/bin/fslmaths $IN $IPOPTS -add ${TMP}_mean -mas ${TMP}_mask $TMP -odt float

# create temporal mean image
${FSLDIR}/bin/fslmaths $TMP -Tmean ${TMP}_mean

# create AR(1) coefficient image
${FSLDIR}/bin/fslmaths $TMP -sub ${TMP}_mean -Tar1 -mas ${TMP}_mask ${TMP}_ar1

# create noise image: masked temporal std image, * 100, divide by mean
${FSLDIR}/bin/fslmaths $TMP -Tstd -mas ${TMP}_mask -mul 100 -div ${TMP}_mean ${TMP}_noise

# get median non-background value from noise and ar1 images
noise=`${FSLDIR}/bin/fslstats ${TMP}_noise -P 50`
ar=`${FSLDIR}/bin/fslstats ${TMP}_ar1 -P 50`
echo $noise $ar

# cleanup
/bin/rm ${TMP}*

