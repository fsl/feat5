#{{{ copyright and setup 

#   GLM - setup GLM design files
#
#   Stephen Smith, Matthew Webster, FMRIB Image Analysis Group
#
#   Copyright (C) 2006 University of Oxford
#
#   TCLCOPYRIGHT

source [ file dirname [ info script ] ]/fslstart.tcl
 
set VARS(history) {}

#}}}

#{{{ GLM GUI

proc glm { w } {
    global fmri FSLDIR USER feat_files VARS PWD HOME tempSpin
 
    #{{{ main window basic setup

feat5:setupdefaults

set fmri(analysis) 2
set fmri(filtering_yn) 0
set fmri(poststats_yn) 0
set fmri(reg_yn) 0
set fmri(wizard_type) 1
set fmri(r_count) 30
set fmri(a_count) 30
set fmri(infeat) 0
set fmri(level) 1

toplevel $w

wm title      $w "GLM Setup"
wm iconname   $w "GLM"
wm iconbitmap $w @${FSLDIR}/tcl/fmrib.xbm

#}}}
    #{{{ main window widgets


optionMenu2 $w.level fmri(level) -command "glm:updatelevel $w"  1 "Timeseries design" 2 "Higher-level / non-timeseries design"
set fmri(npts) 100

LabelSpinBox  $w.npts -textvariable fmri(npts) -label " # timepoints " -range {2 10000 1} -command "$w.npts.spin.e validate; glm:updatenpts $w" -modifycmd "glm:updatenpts $w"
LabelSpinBox  $w.tr -textvariable fmri(tr) -label " TR (s) " -range {0.001 10000 0.25} 
LabelSpinBox  $w.paradigm_hp -textvariable fmri(paradigm_hp) -label " High pass filter cutoff (s) " -range {1.0 10000 5} -width 5 

pack $w.level $w.npts -in $w -side top -anchor w -padx 3 -pady 3

#{{{ button Frame

frame $w.btns
    
button $w.btns.wizard -command "feat5:wizard $w" -text "Wizard"

button $w.btns.save -command "feat_file:setup_dialog $w a a a [namespace current] *.fsf {Save Feat setup} {feat5:write $w 1 0 0} {}" -text "Save"

button $w.btns.load -command "feat_file:setup_dialog $w a a a [namespace current] *.fsf {Load Feat setup} {glm:sourcefsf $w } {}" -text "Load"

button $w.btns.cancel -command "destroy $w" -text "Exit"

button $w.btns.help -command "FmribWebHelp file: ${FSLDIR}/doc/redirects/feat.html" -text "Help"

pack $w.btns.wizard $w.btns.save $w.btns.load $w.btns.cancel $w.btns.help -in $w.btns -side left -expand yes

pack $w.btns -in $w -side bottom -fill x -padx 10 -pady 10

#}}}

#}}}

    glm:updatelevel $w 
}

#}}}
#{{{ glm:sourcefsf

#This will correctly source a design fsf file (can't just put "source" in feat_file as need to have global variables in scope

proc glm:sourcefsf { w filename } {
    global fmri FSLDIR USER feat_files VARS PWD HOME tempSpin
    source $filename
    if { [ info exists fmri(w_model) ] } {
	if { [ winfo exists $fmri(w_model) ] } {
	    destroy $fmri(w_model)
	}
    }
    feat5:setup_model $w
}

#}}}
#{{{ glm:updatelevel

proc glm:updatelevel { w } {
    global fmri

    pack forget $w.tr $w.paradigm_hp
    if { $fmri(level)==1 } {
	$w.npts configure -label " # timepoints "
	pack $w.tr $w.paradigm_hp -in $w -after $w.npts -side top -anchor w -padx 3 -pady 3
    } else {
	set fmri(multiple) $fmri(npts)
	$w.npts configure -label " # inputs "
    }

    if { [ info exists fmri(w_model) ] } {
	if { [ winfo exists $fmri(w_model) ] } {
	    destroy $fmri(w_model)
	}
    }

    feat5:setup_model_vars_simple $w
    feat5:setup_model $w
}

#}}}
#{{{ glm:updatenpts

proc glm:updatenpts { w } {
    global fmri

    if { $fmri(level) == 2 && $fmri(multiple)!=$fmri(npts) } {
	glm:updatelevel $w 
    }
}

#}}}

#{{{ call GUI 

if { ! [ info exists INGUI ] } {
    wm withdraw .
    glm .r
    tkwait window .r
}

#}}}

