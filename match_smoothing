#!/bin/sh

#   match_smoothing - empirical estimation of how much to smooth one image by to match another, in a third space
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 2008 University of Oxford
#
#   SHCOPYRIGHT

res_resample() {
  IN=$1
  SMOOTH=$2
  NEWRES=$3

  # fill input with Gaussian noise and smooth spatially
  ${FSLDIR}/bin/fslmaths $IN -mul 0 -randn -s $SMOOTH $IN -odt float

  # resample to required resolution
  XDIM=`${FSLDIR}/bin/fslval $IN dim1`
  YDIM=`${FSLDIR}/bin/fslval $IN dim2`
  ZDIM=`${FSLDIR}/bin/fslval $IN dim3`
  XPIXDIM=`${FSLDIR}/bin/fslval $IN pixdim1`
  YPIXDIM=`${FSLDIR}/bin/fslval $IN pixdim2`
  ZPIXDIM=`${FSLDIR}/bin/fslval $IN pixdim3`
  NEWXDIM=`echo "$XDIM 1 - $XPIXDIM * $NEWRES / p" | dc`
  NEWYDIM=`echo "$YDIM 1 - $YPIXDIM * $NEWRES / p" | dc`
  NEWZDIM=`echo "$ZDIM 1 - $ZPIXDIM * $NEWRES / p" | dc`
  ${FSLDIR}/bin/fslcreatehd $NEWXDIM $NEWYDIM $NEWZDIM 1 $NEWRES $NEWRES $NEWRES 1 0 0 0 16 ${IN}_new
  ${FSLDIR}/bin/flirt -in ${IN} -ref ${IN}_new -out ${IN}_new -applyxfm

  # create image mask and run smoothest
  ${FSLDIR}/bin/fslmaths ${IN}_new -mul 0 -add 1 ${IN}_new_mask
  smoothness=`${FSLDIR}/bin/smoothest -z ${IN}_new -m ${IN}_new_mask | tail -n 1 | awk '{print $2}'`
}

Usage() {
    echo "Usage: match_smoothing <example_func> <func_smoothing_FWHM_in_mm> <example_structural> <standard_space_resolution>"
    echo "e.g.,  match_smoothing raw_fmri 5 structural 2"
    echo "The output is smoothing sigma needed to be applied to the structural data in its native space"
    exit 1
}

[ "$4" = "" ] && Usage

FUNC=$1
FUNCSMOOTH=`echo "10 k $2 2.355 / p" | dc`
STRUC=$3
RES=$4

tmp=`${FSLDIR}/bin/tmpnam`
/bin/rm $tmp  # remove empty tmp file

${FSLDIR}/bin/fslroi $FUNC $tmp 0 1
res_resample $tmp $FUNCSMOOTH $RES
func_smoothness=$smoothness
#echo Functional data: 1 resel = $func_smoothness standard-space voxels

${FSLDIR}/bin/fslroi $STRUC $tmp 0 1
SMOOTH_LOW=0.1
SMOOTH=2
SMOOTH_LAST=1000000
SMOOTH_HIGH=20
while [ `echo "$SMOOTH $SMOOTH_LAST - $SMOOTH $SMOOTH_LAST - * 100 * 1 / p" | dc` -ne 0 ] ; do
  SMOOTH_LAST=$SMOOTH
  res_resample $tmp $SMOOTH $RES
  #echo Smoothing structural by $SMOOTH gives 1 resel = $smoothness standard-space voxels
  if [ `echo "$smoothness $func_smoothness - 1 / p" | dc` -le 0 ] ; then
    SMOOTH_LOW=$SMOOTH
    SMOOTH=`echo "2 k $SMOOTH_HIGH $SMOOTH - 2 / $SMOOTH + p" | dc`
  else
    SMOOTH_HIGH=$SMOOTH
    SMOOTH=`echo "2 k $SMOOTH $SMOOTH_LOW - 2 / $SMOOTH_LOW + p" | dc`
  fi
done

/bin/rm ${tmp}*

echo $SMOOTH
exit 0
