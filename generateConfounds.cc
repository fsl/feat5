/*    Copyright Matthew Webster (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"

using namespace std;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

int main(int argc, char **argv)
{
  vector<string> fileList;
  ifstream inputFile(argv[1]);
  string currentFile;

  if (!inputFile.is_open()) {
    cerr << "Error opening confound file: " << argv[1] << endl;
    exit(1);
  }

  while( getline( inputFile, currentFile ) )
     fileList.push_back( currentFile );

  Matrix design = read_vest("design.mat");
  int startingColumn=design.Ncols();

  Matrix meanRegressors;
  ofstream fileNames("vef.dat");
  ofstream fileNumbers("ven.dat");
  ofstream meanFile("meanConfounds.dat");

  for( unsigned int currentRegressor=0; currentRegressor < fileList.size(); currentRegressor++ )
  {
    string seperator;
    if ( currentRegressor != fileList.size()-1 )
      seperator=",";
    fileNumbers << currentRegressor+startingColumn+1 << seperator;
    fileNames << "confoundEV"+num2str(currentRegressor+1) << seperator;
    volume4D<double> inputImage;
    read_volume4D(inputImage,fileList[currentRegressor]);
    save_volume4D(inputImage,"confoundEV"+num2str(currentRegressor+1));
    if ( currentRegressor==0 )
      meanRegressors.ReSize(inputImage.tsize(),fileList.size());
    for ( int t=0; t<inputImage.tsize(); t++ )
      meanRegressors(t+1,currentRegressor+1)=inputImage[t].mean();
  }
  meanFile << meanRegressors;
}
