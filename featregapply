#!/bin/sh

#{{{ copyright

#   featregapply - apply registration in a FEAT directory
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 2002-2008 University of Oxford
#
#   TCLCOPYRIGHT

#}}}
#{{{ script setup

# the next line restarts using wish \
exec $FSLTCLSH "$0" "$@"

set INGUI 0

source $env(FSLDIR)/tcl/fslstart.tcl

#}}}
#{{{ proc runregfiles

proc runregfiles { in out } {
    # return 0 if everything exists and is up to date
    catch { exec sh -c "mkdir -p reg_standard/[ file dirname $out ]" } errmsg

    if { ! [ imtest reg_standard/$out ] } {
	return 1
    } elseif { [ file mtime [ imglob -extension reg_standard/$out ] ] < [ file mtime [ imglob -extension $in ] ] || [ file mtime [ imglob -extension reg_standard/$out ] ] < [ file mtime reg/example_func2standard.mat ] } {
	return 1
    } else {
	return 0
    }
}

#}}}
#{{{ proc runflirt

proc runflirt { in ref out space interp datatype } {
    global FSLDIR fmri

    # only do anything at all if things don't exist or need updating
    if { [ runregfiles $in $out ] } {
	set TMPNAM [ fsl:exec "${FSLDIR}/bin/tmpnam frgrot" -n ]

	if { [ imtest reg/highres2standard_warp ] && ( $space == "example_func" || $space == "highres" ) } {

	    set PREMAT ""
	    if { $space == "example_func" } {
		set PREMAT "--premat=reg/example_func2highres.mat"
	    }	    
	    if { $interp == "nearestneighbour" } {
		set interp nn
	    }
	    fsl:exec "${FSLDIR}/bin/applywarp --ref=$ref --in=$in --out=$TMPNAM --warp=reg/highres2standard_warp $PREMAT --interp=$interp"

	} else {

	    set FLIRTINIT ""
	    if { $space != "standard" } {
		set FLIRTINIT "-init reg/${space}2standard.mat"
	    }

	    if { [ info exists fmri(singleSlice) ] && $fmri(singleSlice) == "1" } { 
	       fsl:exec "${FSLDIR}/bin/flirt -2D -ref $ref -in $in -out $TMPNAM -applyxfm $FLIRTINIT -interp $interp -datatype $datatype"
	    } else {
	       fsl:exec "${FSLDIR}/bin/flirt -ref $ref -in $in -out $TMPNAM -applyxfm $FLIRTINIT -interp $interp -datatype $datatype"
	    }

	}

	immv $TMPNAM reg_standard/$out
	fsl:exec "/bin/rm $TMPNAM" -n
    }
}

#}}}
#{{{ process options

if { [ lindex $argv 0 ] == "" } {
    puts "
Usage: featregapply <feat_directory> \[-f\] \[-c\] \[-l <image>\] \[-s <image>\] \[-r <res>\] \[-e\]
  -f : force featregapply to run even if already run on this FEAT directory
  -c : cleanup, i.e. remove all featregapply output
  -l <image> : upsample functional-space <image> to standard space using trilinear interpolation
  -s <image> : upsample functional-space <image> to standard space using spline (like sinc) interpolation
  -r <res> : specify the standard space resolution for melodic (e.g. 3 for 3mm)
  -e : exclude filtered func when processing melodic directories (for FEAT directories filtered func is never processed)"

    exit 1
}

cd [ lindex $argv 0 ]

if { [ file exists design.lev ] } {
    exit 0
}
if { ! [ file exists reg/example_func2standard.mat ] } {
    puts "Error - registration has not been run for [ pwd ]"
    exit 1
}

feat5:setupdefaults
feat5:load -1 1 design.fsf


# for older first-level FEAT dirs: create mean_func if it's not already there
if { [ imtest filtered_func_data ] && ! [ imtest mean_func ] } {
    fsl:exec "${FSLDIR}/bin/fslmaths filtered_func_data -Tmean mean_func"
}

set excludeFiltered 0
# Freshen or cleanup old reg_standard
for { set i 1 } { $i < [ llength $argv ] } { incr i 1 } {
    if { ! [ string compare [ lindex $argv $i ] "-f" ] } {
	fsl:exec "/bin/rm -rf reg_standard"
    } elseif { ! [ string compare [ lindex $argv $i ] "-e" ] } {
       set excludeFiltered 1
    } elseif { ! [ string compare [ lindex $argv $i ] "-c" ] } {
	fsl:exec "/bin/rm -rf reg_standard"
	exit 0
    } elseif { ! [ string compare [ lindex $argv $i ] "-r" ] } {
	incr i 1
	set fmri(regstandard_res) [ lindex $argv $i ]
    }
}
catch { exec sh -c "mkdir -p reg_standard/reg reg_standard/stats" } errmsg

set REF reg/standard
if { [ info exists fmri(inmelodic) ] && $fmri(inmelodic) } {
    set REF reg_standard/standard

    if { ! [ info exists fmri(regstandard_res) ] || $fmri(regstandard_res) == 0 } { # Just take a copy of the standard space image
      fsl:exec "${FSLDIR}/bin/imcp reg/standard reg_standard/standard"
    } elseif { $fmri(regstandard_res)>0.0001 } {
      fsl:exec "${FSLDIR}/bin/flirt -ref reg/standard -in reg/standard -out reg_standard/standard -applyisoxfm $fmri(regstandard_res)"
    } else {
      puts "Error - standard space resolution too small - tried to use $fmri(regstandard_res) mm"
      exit 1
    }
}

for { set i 1 } { $i < [ llength $argv ] } { incr i 1 } {
    if { ! [ string compare [ lindex $argv $i ] "-l" ] } {
      incr i 1
      set theim [ lindex $argv $i ]
      runflirt $theim $REF $theim example_func trilinear float
    } elseif { ! [ string compare [ lindex $argv $i ] "-s" ] } {
      incr i 1
      set theim [ lindex $argv $i ]
      runflirt $theim $REF $theim example_func spline float
    }
}



#}}}
#{{{ apply the registration


# MELODIC upsampling
if { [ info exists fmri(inmelodic) ] && $fmri(inmelodic) } {

    set BGIMAGE ""
    switch $fmri(bgimage) {
	1 -
	2 { set BGIMAGE highres }
	3 -
	4 { set BGIMAGE example_func }
    }
    if { [ imtest reg/$BGIMAGE ] } {
	runflirt reg/$BGIMAGE $REF bg_image $BGIMAGE spline float
    } else {
	imcp reg_standard/standard reg_standard/bg_image
    }
    if { !$excludeFiltered } {
      runflirt filtered_func_data $REF filtered_func_data example_func trilinear float
    }
    runflirt mask $REF mask example_func trilinear float
    fsl:exec "${FSLDIR}/bin/fslmaths reg_standard/mask -bin reg_standard/mask -odt char"       
} else {
    foreach input [ imglob example_func mean_func ] {
	runflirt $input $REF $input example_func spline float
    }
    foreach input [ imglob stats/cope* stats/varcope* stats/tdof_t* ] {
	runflirt $input $REF $input example_func trilinear float
    }
    foreach input [ imglob mask ] {
	runflirt $input $REF $input example_func nearestneighbour char
    }

    if { [ file exists reg/highres2standard.mat ] } {
	runflirt reg/highres $REF reg/highres highres spline float
    }
}

#}}}
