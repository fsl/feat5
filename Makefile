include $(FSLCONFDIR)/default.mk

PROJNAME  = feat5
XFILES    = cutoffcalc feat_model tsplot prewhiten \
            generateConfounds popp pnm_evs
SOFILES   = libfsl-featlib.so
RUNTCLS   = Feat Renderhighres Featquery Glm Pnm
SCRIPTS   = easythresh feat mccutup renderhighres \
            featregapply featquery fslerrorreport \
            invfeatreg updatefeatreg estnoise \
            perfusion_subtract match_smoothing \
            fsl_motion_outliers feat_gm_prepare \
            mp_diffpow.sh pnm_stage1 mainfeatreg \
            checkFEAT setFEAT autoaq
DATAFILES = data/*
LIBS      = -lfsl-newimage -lfsl-miscplot \
            -lfsl-miscpic -lfsl-miscmaths -lfsl-cprob \
            -lfsl-utils -lfsl-NewNifti -lfsl-znz -lgdc \
            -lgd -lpng

all: ${XFILES} ${SOFILES}

custominstall:
	@if [ ! -d ${DESTDIR}/etc/fslconf ] ; then ${MKDIR} -p ${DESTDIR}/etc/fslconf ; ${CHMOD} g+w ${DESTDIR}/etc/fslconf ; fi
	${CP} -f featconf ${DESTDIR}/etc/fslconf/feat.tcl

libfsl-featlib.so: featlib.cc
	$(CXX) $(CXXFLAGS) -shared -o $@ $^ $(LDFLAGS)

%: %.o libfsl-featlib.so
	$(CXX) $(CXXFLAGS) -o $@ $< -lfsl-featlib $(LDFLAGS)
