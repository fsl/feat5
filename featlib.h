/* {{{ Copyright etc. */

/*  tsplot - FMRI time series and model plotting

    Stephen Smith and Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2002 University of Oxford  */

/*  CCOPYRIGHT */

/* }}} */
/* {{{ defines, includes and typedefs */

#include <vector>

#include "newimage/newimageall.h"

void establish_pwfilter(const NEWMAT::ColumnVector&, NEWMAT::ColumnVector&, int, int);
void prewhiten_model(const NEWMAT::ColumnVector&,std::vector<double>& model,std::vector<double>& pwmodel, int, int);
void prewhiten_timeseries(const NEWMAT::ColumnVector&, const NEWMAT::ColumnVector&, NEWMAT::ColumnVector&, int);
std::vector<double> read_model(const std::string& filename, int *, int *);
void read_ftests(const std::string& filename, int *);
bool read_triggers(const std::string& filename,std::vector<double>& triggers, int, int);

/* }}} */
