#{{{ copyright and setup

#   renderhighres - render stats onto high res structurals
#
#   Stephen Smith, FMRIB Image Analysis Group
#
#   Copyright (C) 1999-2005 University of Oxford
#
#   TCLCOPYRIGHT

source [ file dirname [ info script ] ]/fslstart.tcl
#bind Button <1> "focus %W ; [bind Button <1>] "
#tk_focusFollowsMouse
set VARS(history) {}

#}}}
#{{{ feat5:renderhighres

proc feat5:renderhighres { w } {
    global PXHOME FSLDIR VARS argc argv PWD entries rendervars

    #{{{ setup main window

toplevel $w

wm title      $w "Upsample+overlay FEAT stats"
wm iconname   $w "FEAT upsample"
wm iconbitmap $w @${FSLDIR}/tcl/fmrib.xbm

frame $w.f
pack $w.f -in $w -side top



FileEntry $w.f.featdir -textvariable entries($w,1) -label "Select a FEAT directory" -title "Select a FEAT directory"  -width 20 -filedialog directory -filetypes "*.feat" -dirasfile "design.fsf" 
#FSLFileEntry $w.f.featdir -varia entries($w,1)-pattern "*.feat" -directory "~" -label "Select a FEAT directory" -title "Select a FEAT directory" -width 20 -dirasfile "design.fsf" -filterhist VARS(history)

frame $w.f.space
label $w.f.space.label -text "Space to upsample to: "
set rendervars(space) standard
radiobutton $w.f.space.highres  -text "main structural"  -value highres  -variable rendervars(space)
radiobutton $w.f.space.standard -text "standard"         -value standard -variable rendervars(space)
pack $w.f.space.label $w.f.space.highres $w.f.space.standard -in $w.f.space -side left -anchor w

frame $w.f.background
label $w.f.background.label -text "Background image: "
set rendervars(background) highres
radiobutton $w.f.background.highres  -text "main structural"  -value highres  -variable rendervars(background)
radiobutton $w.f.background.standard -text "standard"         -value standard -variable rendervars(background)
pack $w.f.background.label $w.f.background.highres $w.f.background.standard -in $w.f.background -side left -anchor w

frame $w.f.autothresh
label $w.f.autothresh.label -text "Colour-code using existing post-threshold range"
set rendervars(autothresh) 1
checkbutton $w.f.autothresh.yn -variable rendervars(autothresh) -command "feat5:autothresh_update $w"
pack $w.f.autothresh.label $w.f.autothresh.yn -in $w.f.autothresh -side left -anchor w

frame $w.f.minmax
set rendervars(zmin) 1


LabelSpinBox $w.f.zmin -label "  Colour-code range:  Min"  -textvariable rendervars(zmin) -range {0 10000 1 } -width 4
#tixControl $w.f.zmin -label "  Colour-code range:  Min" -variable rendervars(zmin) -step 1 -min 0 -selectmode immediate -options { entry.width 4 }
set rendervars(zmax) 15

LabelSpinBox $w.f.zmax -label "Max"  -textvariable rendervars(zmax) -range {0 10000 1 } -width 4
#tixControl $w.f.zmax -label "Max" -variable rendervars(zmax) -step 1 -min 0 -selectmode immediate -options { entry.width 4 }
pack $w.f.zmin $w.f.zmax -in $w.f.minmax -side left -anchor w -padx 5

pack $w.f.featdir $w.f.space $w.f.background $w.f.autothresh -in $w.f -padx 5 -pady 5 -anchor w

#}}}
    #{{{ Button Frame

frame $w.btns
frame $w.btns.b -relief raised -borderwidth 1
    
button $w.apply -command "feat5:renderhighres_proc $w" -text "Go"

button $w.cancel -command "destroy $w" -text "Exit"

pack $w.btns.b -side bottom -fill x -padx 3 -pady 3
pack $w.apply $w.cancel -in $w.btns.b -side left -expand yes -padx 3 -pady 3 -fill y

pack $w.btns -in $w -side bottom -fill x

#}}}
}

#}}}
#{{{ feat5:autothresh_update

proc feat5:autothresh_update { w } {
    global rendervars

    pack forget $w.f.minmax

    if { $rendervars(autothresh) == 0 } {
	pack $w.f.minmax -in $w.f -padx 5 -pady 5 -anchor w -after $w.f.autothresh
    }

}

#}}}
#{{{ feat5:renderhighres_proc

proc feat5:renderhighres_proc { w } {

    global FSLDIR entries rendervars

    set FD $entries($w,1)

    puts "Processing $FD"

    fsl:exec "${FSLDIR}/bin/renderhighres $FD $rendervars(space) $rendervars(background) $rendervars(autothresh) $rendervars(zmin) $rendervars(zmax)"

    puts "Done"
}

#}}}
#{{{ call GUI and wait

wm withdraw .
feat5:renderhighres .r
tkwait window .r

#}}}

